import inspect

import hello

TXT_BRIGHT_RED="\x1b[91m"
TXT_BRIGHT_GREEN="\x1b[92m"
TXT_BRIGHT_CYAN="\x1b[96m"
TXT_CLEAR="\x1b[0m"

def check_hello_function():
    print("  >> " + TXT_BRIGHT_CYAN + "INFO: checking number of functions in module hello" + TXT_CLEAR)
    functions = inspect.getmembers(hello, lambda f: inspect.isfunction(f))
    if len(functions) != 1:
        print("  >> " + TXT_BRIGHT_RED + "ERROR: your script must define only one function" + TXT_CLEAR)
        return False
    count = len(inspect.signature(functions[0][1]).parameters)
    if count != 2:
        print("  >> " + TXT_BRIGHT_RED + "ERROR: your function named hello() must take exactly 2 arguments, yours has " + str(count) + TXT_CLEAR)
        return False
    print("  >> " + TXT_BRIGHT_GREEN + "SUCCESS: you have only one function, named hello(), with exactly 2 arguments" + TXT_CLEAR)
    return True


if __name__ == "__main__":
    if not check_hello_function():
        exit(-1)
    exit(0)

