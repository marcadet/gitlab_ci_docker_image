#!/usr/bin/env bash

TXT_WHITE="\e[37m"
TXT_BRIGHT_RED="\e[91m"
TXT_BRIGHT_GREEN="\e[92m"
TXT_BRIGHT_YELLOW="\e[93m"
TXT_BRIGHT_CYAN="\e[96m"
TXT_CLEAR="\e[0m"

TXT_BACKGROUND_GREEN="\e[42m"
TXT_BACKGROUND_DEFAULT="\e[49m"


echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: now, you will have to modify your existing python script so that the following tests succeed.${TXT_CLEAR}"
echo -e "  >> ${TXT_BRIGHT_YELLOW}WARNING: the previous tests will still be executed and must continue to succeed.${TXT_CLEAR}"

########################################################

ARG_COMMAND="hello.py"
COMMAND="${TXT_WHITE}python ${ARG_COMMAND}"
echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking running your script with: ${COMMAND}${TXT_CLEAR}"
echo "hello world !" >expected.txt
python ${ARG_COMMAND} >result.txt
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: an error occurred while running your script with: ${COMMAND}${TXT_CLEAR}"
    exit -1
fi
cmp -s expected.txt result.txt
if [[ $? -ne 0 ]]; then
    echo -e -n "  >> ${TXT_BRIGHT_RED}ERROR: the expected output when running ${COMMAND} is: ${TXT_CLEAR}"
    cat expected.txt
    echo -e -n "     ${TXT_BRIGHT_RED}       your program gives as output: ${TXT_CLEAR}"
    cat result.txt
    exit -1
fi
echo -e -n "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your script gives the right output when running as: ${COMMAND}: ${TXT_CLEAR}"
cat result.txt

########################################################

ARG_COMMAND="hello.py my friend"
COMMAND="${TXT_WHITE}python ${ARG_COMMAND}"
echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking running your script with: ${COMMAND}${TXT_CLEAR}"
echo "hello my friend !" >expected.txt
python ${ARG_COMMAND} >result.txt
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: an error occurred while running your script with: ${COMMAND}${TXT_CLEAR}"
    exit -1
fi
cmp -s expected.txt result.txt
if [[ $? -ne 0 ]]; then
    echo -e -n "  >> ${TXT_BRIGHT_RED}ERROR: the expected output when running ${COMMAND} is: ${TXT_CLEAR}"
    cat expected.txt
    echo -e -n "     ${TXT_BRIGHT_RED}       your program gives as output: ${TXT_CLEAR}"
    cat result.txt
    exit -1
fi
echo -e -n "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your script gives the right output when running as: ${COMMAND}: ${TXT_CLEAR}"
cat result.txt

########################################################

ARG_COMMAND="hello.py -fr"
COMMAND="${TXT_WHITE}python ${ARG_COMMAND}"
echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking running your script with: ${COMMAND}${TXT_CLEAR}"
echo "bonjour le monde !" >expected.txt
python ${ARG_COMMAND} >result.txt
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: an error occurred while running your script with: ${COMMAND}${TXT_CLEAR}"
    exit -1
fi
cmp -s expected.txt result.txt
if [[ $? -ne 0 ]]; then
    echo -e -n "  >> ${TXT_BRIGHT_RED}ERROR: the expected output when running ${COMMAND} is: ${TXT_CLEAR}"
    cat expected.txt
    echo -e -n "     ${TXT_BRIGHT_RED}       your program gives as output: ${TXT_CLEAR}"
    cat result.txt
    exit -1
fi
echo -e -n "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your script gives the right output when running as: ${COMMAND}: ${TXT_CLEAR}"
cat result.txt

########################################################

ARG_COMMAND="hello.py -fr le monde"
COMMAND="${TXT_WHITE}python ${ARG_COMMAND}"
echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking running your script with: ${COMMAND}${TXT_CLEAR}"
echo "bonjour le monde !" >expected.txt
python ${ARG_COMMAND} >result.txt
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: an error occurred while running your script with: ${COMMAND}${TXT_CLEAR}"
    exit -1
fi
cmp -s expected.txt result.txt
if [[ $? -ne 0 ]]; then
    echo -e -n "  >> ${TXT_BRIGHT_RED}ERROR: the expected output when running ${COMMAND} is: ${TXT_CLEAR}"
    cat expected.txt
    echo -e -n "     ${TXT_BRIGHT_RED}       your program gives as output: ${TXT_CLEAR}"
    cat result.txt
    exit -1
fi
echo -e -n "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your script gives the right output when running as: ${COMMAND}: ${TXT_CLEAR}"
cat result.txt

########################################################

TESTED_WORD=`shuf -n1 /usr/share/dict/words`
ARG_COMMAND="hello.py -fr ${TESTED_WORD}"
COMMAND="${TXT_WHITE}python ${ARG_COMMAND}"
echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking running your script with: ${COMMAND}${TXT_CLEAR}"
echo "bonjour ${TESTED_WORD} !" >expected.txt
python ${ARG_COMMAND} >result.txt
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: an error occurred while running your script with: ${COMMAND}${TXT_CLEAR}"
    exit -1
fi
cmp -s expected.txt result.txt
if [[ $? -ne 0 ]]; then
    echo -e -n "  >> ${TXT_BRIGHT_RED}ERROR: the expected output when running ${COMMAND} is: ${TXT_CLEAR}"
    cat expected.txt
    echo -e -n "     ${TXT_BRIGHT_RED}       your program gives as output: ${TXT_CLEAR}"
    cat result.txt
    exit -1
fi
echo -e -n "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your script gives the right output when running as: ${COMMAND}: ${TXT_CLEAR}"
cat result.txt

########################################################

ARG_COMMAND="hello.py -en"
COMMAND="${TXT_WHITE}python ${ARG_COMMAND}"
echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking running your script with: ${COMMAND}${TXT_CLEAR}"
echo "hello world !" >expected.txt
python ${ARG_COMMAND} >result.txt
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: an error occurred while running your script with: ${COMMAND}${TXT_CLEAR}"
    exit -1
fi
cmp -s expected.txt result.txt
if [[ $? -ne 0 ]]; then
    echo -e -n "  >> ${TXT_BRIGHT_RED}ERROR: the expected output when running ${COMMAND} is: ${TXT_CLEAR}"
    cat expected.txt
    echo -e -n "     ${TXT_BRIGHT_RED}       your program gives as output: ${TXT_CLEAR}"
    cat result.txt
    exit -1
fi
echo -e -n "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your script gives the right output when running as: ${COMMAND}: ${TXT_CLEAR}"
cat result.txt

########################################################

ARG_COMMAND="hello.py -en my friend"
COMMAND="${TXT_WHITE}python ${ARG_COMMAND}"
echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking running your script with: ${COMMAND}${TXT_CLEAR}"
echo "hello my friend !" >expected.txt
python ${ARG_COMMAND} >result.txt
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: an error occurred while running your script with: ${COMMAND}${TXT_CLEAR}"
    exit -1
fi
cmp -s expected.txt result.txt
if [[ $? -ne 0 ]]; then
    echo -e -n "  >> ${TXT_BRIGHT_RED}ERROR: the expected output when running ${COMMAND} is: ${TXT_CLEAR}"
    cat expected.txt
    echo -e -n "     ${TXT_BRIGHT_RED}       your program gives as output: ${TXT_CLEAR}"
    cat result.txt
    exit -1
fi
echo -e -n "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your script gives the right output when running as: ${COMMAND}: ${TXT_CLEAR}"
cat result.txt

########################################################

TESTED_WORD=`shuf -n1 /usr/share/dict/words`
ARG_COMMAND="hello.py -en ${TESTED_WORD}"
COMMAND="${TXT_WHITE}python ${ARG_COMMAND}"
echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking running your script with: ${COMMAND}${TXT_CLEAR}"
echo "hello ${TESTED_WORD} !" >expected.txt
python ${ARG_COMMAND} >result.txt
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: an error occurred while running your script with: ${COMMAND}${TXT_CLEAR}"
    exit -1
fi
cmp -s expected.txt result.txt
if [[ $? -ne 0 ]]; then
    echo -e -n "  >> ${TXT_BRIGHT_RED}ERROR: the expected output when running ${COMMAND} is: ${TXT_CLEAR}"
    cat expected.txt
    echo -e -n "     ${TXT_BRIGHT_RED}       your program gives as output: ${TXT_CLEAR}"
    cat result.txt
    exit -1
fi
echo -e -n "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your script gives the right output when running as: ${COMMAND}: ${TXT_CLEAR}"
cat result.txt

########################################################

ARG_COMMAND="hello.py -ac"
COMMAND="${TXT_WHITE}python ${ARG_COMMAND}"
echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking running your script with: ${COMMAND}${TXT_CLEAR}"
echo "unknown language: ac" >expected.txt
python ${ARG_COMMAND} >result.txt
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: an error occurred while running your script with: ${COMMAND}${TXT_CLEAR}"
    exit -1
fi
cmp -s expected.txt result.txt
if [[ $? -ne 0 ]]; then
    echo -e -n "  >> ${TXT_BRIGHT_RED}ERROR: the expected output when running ${COMMAND} is: ${TXT_CLEAR}"
    cat expected.txt
    echo -e -n "     ${TXT_BRIGHT_RED}       your program gives as output: ${TXT_CLEAR}"
    cat result.txt
    exit -1
fi
echo -e -n "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your script gives the right output when running as: ${COMMAND}: ${TXT_CLEAR}"
cat result.txt

########################################################

TESTED_WORD=`shuf -n1 /usr/share/dict/words`
ARG_COMMAND="hello.py -zx ${TESTED_WORD}"
COMMAND="${TXT_WHITE}python ${ARG_COMMAND}"
echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking running your script with: ${COMMAND}${TXT_CLEAR}"
echo "unknown language: zx" >expected.txt
python ${ARG_COMMAND} >result.txt
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: an error occurred while running your script with: ${COMMAND}${TXT_CLEAR}"
    exit -1
fi
cmp -s expected.txt result.txt
if [[ $? -ne 0 ]]; then
    echo -e -n "  >> ${TXT_BRIGHT_RED}ERROR: the expected output when running ${COMMAND} is: ${TXT_CLEAR}"
    cat expected.txt
    echo -e -n "     ${TXT_BRIGHT_RED}       your program gives as output: ${TXT_CLEAR}"
    cat result.txt
    exit -1
fi
echo -e -n "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your script gives the right output when running as: ${COMMAND}: ${TXT_CLEAR}"
cat result.txt

########################################################

cp /sip/sip_test_extended_hello.py .
python sip_test_extended_hello.py
if [[ $? -ne 0 ]]; then
    exit -1
fi

echo -e "  >> ${TXT_BACKGROUND_GREEN}SUCCESS: your python script passes all the tests.${TXT_BACKGROUND_DEFAULT}"

exit 0

