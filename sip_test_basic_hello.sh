#!/usr/bin/env bash

TXT_WHITE="\e[37m"
TXT_BRIGHT_RED="\e[91m"
TXT_BRIGHT_GREEN="\e[92m"
TXT_BRIGHT_CYAN="\e[96m"
TXT_CLEAR="\e[0m"

echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking existence of file hello.py${TXT_CLEAR}"
if [[ ! -e hello.py ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: your python script must be called hello.py${TXT_CLEAR}"
    exit -1
fi
echo -e "  >> ${TXT_BRIGHT_GREEN}SUCCESS: file hello.py is present${TXT_CLEAR}"

########################################################

echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking syntax of file hello.py${TXT_CLEAR}"
python -m py_compile hello.py
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: your file hello.py is not a valid python script${TXT_CLEAR}"
    exit -1
fi
echo -e "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your file hello.py is a valid python script${TXT_CLEAR}"

########################################################

ARG_COMMAND="hello.py world"
COMMAND="${TXT_WHITE}python ${ARG_COMMAND}"
echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking running your script with: ${COMMAND}${TXT_CLEAR}"
echo "hello world !" >expected.txt
python ${ARG_COMMAND} >result.txt
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: an error occurred while running your script with: ${COMMAND}${TXT_CLEAR}"
    exit -1
fi
cmp -s expected.txt result.txt
if [[ $? -ne 0 ]]; then
    echo -e -n "  >> ${TXT_BRIGHT_RED}ERROR: the expected output when running ${COMMAND} is: ${TXT_CLEAR}"
    cat expected.txt
    echo -e -n "     ${TXT_BRIGHT_RED}       your program gives as output: ${TXT_CLEAR}"
    cat result.txt
    exit -1
fi
echo -e -n "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your script gives the right output when running as: ${COMMAND}: ${TXT_CLEAR}"
cat result.txt

########################################################

TESTED_WORD=`shuf -n1 /usr/share/dict/words`
ARG_COMMAND="hello.py ${TESTED_WORD}"
COMMAND="${TXT_WHITE}python ${ARG_COMMAND}"
echo -e "  >> ${TXT_BRIGHT_CYAN}INFO: checking running your script with: ${COMMAND}${TXT_CLEAR}"
echo "hello ${TESTED_WORD} !" >expected.txt
python ${ARG_COMMAND} >result.txt
if [[ $? -ne 0 ]]; then
    echo -e "  >> ${TXT_BRIGHT_RED}ERROR: an error occurred while running your script with: ${COMMAND}${TXT_CLEAR}"
    exit -1
fi
cmp -s expected.txt result.txt
if [[ $? -ne 0 ]]; then
    echo -e -n "  >> ${TXT_BRIGHT_RED}ERROR: the expected output when running ${COMMAND} is: ${TXT_CLEAR}"
    cat expected.txt
    echo -e -n "     ${TXT_BRIGHT_RED}       your program gives as output: ${TXT_CLEAR}"
    cat result.txt
    exit -1
fi
echo -e -n "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your script gives the right output when running as: ${COMMAND}: ${TXT_CLEAR}"
cat result.txt

########################################################

cp /sip/sip_test_basic_hello.py .
python sip_test_basic_hello.py
if [[ $? -ne 0 ]]; then
    exit -1
fi

echo -e "  >> ${TXT_BRIGHT_GREEN}SUCCESS: your python script gives the result expected at the end of the first \
part of this lab, the computer quiz.${TXT_CLEAR}"

exit 0

