# A Python repository can be tested with the following .gitlab-ci.yml file 

```yml
image: gitlab-student.centralesupelec.fr:4567/marcadet/gitlab_ci_docker_image:latest

stages:
    - test_basic
    - test_extended
 
test_basic:
    stage: test_basic
    script:
        - /sip/sip_test_basic_hello.sh
    tags:
        - docker
 
test_extended:
    stage: test_extended
    script:
        - /sip/sip_test_extended_hello.sh
    tags:
        - docker
```

# Build

docker login gitlab-student.centralesupelec.fr:4567

docker build -t gitlab-student.centralesupelec.fr:4567/marcadet/gitlab_ci_docker_image .

docker push gitlab-student.centralesupelec.fr:4567/marcadet/gitlab_ci_docker_image
